const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	orderDetails : [{
		userId : {
			type : String,
			required : [true, "User ID is required."]
		},
		productId : {
			type : String,
			required : [true, "Product ID is required."]
		},
		purchasedOn : {
			type : Date,
			default : new Date()
		},
		totalAmount : {
			type : Number,
			default : 0
		}
	}]
});

module.exports = mongoose.model("Order", orderSchema);