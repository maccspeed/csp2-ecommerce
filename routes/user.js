const express = require('express');
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id})
	.then(resultFromController => res.send(resultFromController));
});

router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body)
	.then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body)
	.then(resultFromController => res.send(resultFromController));
});

// Route for authenticating a user
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	userController.authenticateUser(user, req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
});

// Route for creating an order
router.post("/checkout", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	userController.createOrder(user, req.body)
	.then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all orders
router.get("/orders", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	userController.retrieveAllOrders(user)
	.then(resultFromController => res.send(resultFromController));
});

// Retrieve Authenticated User's Orders
router.get("/myOrders", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	userController.retrieveSpecificOrder(user, req.body)
	.then(resultFromController => res.send(resultFromController));
});

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;