const express = require("express");
const router = express.Router();
const productController = require("../controllers/product.js");
const auth = require("../auth");

// create/add product
router.post("/", auth.verify, (req, res) => {
	
	const user = auth.decode(req.headers.authorization);

	productController.createProduct(user, req.body)
	.then(resultFromController => res.send(resultFromController));
})

// retrieve active products
router.get("/", (req, res) => {
	productController.retrieveActiveProducts()
	.then(resultFromController => res.send(resultFromController));
})

// retrieve a product
router.get("/:productId", (req, res) => {
	productController.retrieveProduct(req.params)
	.then(resultFromController => res.send(resultFromController));
})

// update a product
router.put("/:productId", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);

	productController.updateProduct(user, req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
})

// archive/delete specific product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	
	productController.archiveProduct(user, req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
})

module.exports = router;