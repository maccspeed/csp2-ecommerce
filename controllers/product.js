const Product = require("../models/Product");

// create/add new product
module.exports.createProduct = async (user, reqBody) => {
	if (user.isAdmin) {
		let newProduct = new Product({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		})

		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	}
	else {
		return "You have no power here!";
	}
}

// retrieve active products
module.exports.retrieveActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.retrieveProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// update a product
module.exports.updateProduct = (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		let updateProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
		.then((course, error) => {
			if (error) { return false; } else { return true; }
		})
	}
	else {
		return "You have no power here!";
	}
}

// archive a product
module.exports.archiveProduct = async (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		let archiveProduct = {
			isActive: reqBody.isActive
		}
		return Product.findByIdAndUpdate(reqParams.productId, archiveProduct)
		.then((product, error) => {
			if (error) { return false; } else { return true; }
		})
	}
	else {
		return "You have no power here!"
	}
}