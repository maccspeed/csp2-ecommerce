const User = require("../models/User");
const Order = require("../models/Order");
// for password encryption
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmail = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
        return (result.length > 0) ? true: false;
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		}
		else {
			return true;
		}
	})
}

module.exports.authenticateUser = async (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		let authUser = {
			isAdmin: reqBody.isAdmin
		}

		return User.findByIdAndUpdate(reqParams.userId, authUser)
		.then((course, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	}
	else {
		return ("You have no power here");
	}
}


// Create new order
module.exports.createOrder = async (user, reqBody) => {
    if (user.isAdmin) {
        return "You are not allowed to create an order."
    }
    else {
        let newOrder = new Order({
            orderDetails : [{
				userId: reqBody.userId,
            	productId: reqBody.productId
			}]
        });

        return newOrder.save().then((user, error) => {
            if (error) {
                return false;
            }
            else {
                return true;
            }
        });
    }
}

module.exports.retrieveAllOrders = async (user) => {
	if (user.isAdmin) {
		return Order.find({}).then(result => {
			return result;
		})
	}
	else {
		return "You have no power here!";
	}
}

module.exports.retrieveSpecificOrder = (user, reqBody) => {
	if (user.isAdmin == false) {
		return Order.find({userId: reqBody.userId});
	}
	else {
		return "You're not allowed to access this information.";
	}
};

// User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false;
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) };
			} else {
				return false;
			}
		}
	})
}