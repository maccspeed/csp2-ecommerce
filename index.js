const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")

const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://dbmarcomacdon:kfu4zemo12UIq9FE@wdc028-course-booking.9zble.mongodb.net/ecommerceDB?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"));

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Defines the "users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);

app.use("/products", productRoutes);

// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`);
});